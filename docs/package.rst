ATMOS package
=============

Installation
~~~~~~~~~~~~

ATMOS requires Python 3.9 or above.

The easiest way to use ATMOS at the moment is to `download the python wheel 
from here <https://gitlab.com/NC-AFM-at-UniLeeds/atmos/-/jobs/artifacts/main/browse/dist?job=pack>`_. 

To install it, run::

    pip install ATMOS-*.whl

of the version you want to have.

If you are reinstalling ATMOS, you can do::

    pip install --no-deps --force-reinstall ATMOS-*.whl

in order to avoid reinstalling dependencies and forcing a reinstall in your python environment.

Note:
^^^^
At present, because of a `bug in numpy's installation mechanism using pip <https://github.com/numpy/numpy/issues/20039>`_,
it is recommended to use a (new) `conda environment <https://docs.conda.io/en/latest/miniconda.html>`_ and installing `numpy` using 
`conda`. While we are at it, maybe you can install `pandas` and `xarray` using the
same, followed by the `pip install ...` command to install the remaining::

    conda create -n atmos_env python numpy pandas xarray
    conda activate atmos_env
    pip install ATMOS-*.whl

After Installation, you can start using it by importing it. The recommended way
of importing ATMOS is::

    from atmos import Assembly


Contributing
~~~~~~~~~~~~

ATMOS is a nascent project with the following features coming steadily by the end of 
2021 (they are in process of being ported to ATMOS from BaseCode):

  - Writing (extended) XYZ files
  - Wrapper around `Grimme's D3/D4 code <https://www.chemie.uni-bonn.de/pctc/mulliken-center/software/dftd4/d4-a-generally-applicable-atomic-charge-dependent-london-dispersion-correction>`_.

Two new features that are under development are
  
  - Interactive visualization of stored quantities (most likely using Streamlit)
  - Reading DoS output files of FHIaims
  - A simple command line interface (taking shape in form of '<<' operator)

If you have any particular features that you would like to have, feel free to open an `issue 
with the online repository here <https://gitlab.com/NC-AFM-at-UniLeeds/atmos/-/issues>`_.

Better still, if you feel you can contribute to the project, make a pull request 
with your contributions. Please try and include as much documentation as possible
in the code because ATMOS documentation resides alongside code. (ATMOS uses Google
style docstrings). Any contributions to project would be considered to be made
under the same license as rest of the project.

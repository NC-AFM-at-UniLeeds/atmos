.. ATMOS documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ATMOS
=====

Atoms, Tips and Molecules over Surfaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A python package to help you when doing atomistic simulations for surface science.

.. toctree::
   :maxdepth: 1
   :caption: Documentation

   package
   atmos

Motivation
----------

The origins of ATMOS are in another project called BaseCode. BaseCode was 
developed for reading geometries and outputs from ab-initio simulation 
packages such as `VASP <https://vasp.at>`_ and `FHI-aims <https://fhi-aims.org>`_ 
(though it is extensible in terms of adding more file formats). Internally 
all data in BaseCode was held as numpy arrays associated with an instance of 
a class called `System`.

In hindsight (over 2 years of use and learning a lot more about making `better 
research software <https://the-turing-way.netlify.app/reproducible-research/reproducible-research.html>`_), 
we know that BaseCode has a number of drawbacks, the four major ones being:

- Extensibility: BaseCode had pre-defined quantity holders that were attached 
  to instances of `System`. For example, BaseCode was programmed to hold a 
  quantity called 'charge' for every atom. But there are multiple ways of 
  `calculating charge <https://en.wikipedia.org/wiki/Partial_charge>`_
  on atom. In such a situation BaseCode forced users to make a choice when 
  reading output from a simulation code. Similarly, attaching any new quantity 
  (whose holder was not pre-defined) in BaseCode was not possible unless one 
  modified the source code. This was becoming unmanagable as users became 
  interested in more and more quantities from the output.

- Saving data: There was no provision for saving the data. To re-analyze, one 
  had to re-read output of the simulation code again. Pickling instances of
  `System` could have worked but that's not portable in the long run. 

- BaseCode was not made with `OSI <https://opensource.org>`_ in mind.

- Neither `BaseCode` nor `System` were brilliant choices as names.

The main goal of ATMOS is to solve these problems while easing user experience 
when it comes to data handling. After trying different approaches, the 
developers have decided to use `Xarrays <http://xarray.pydata.org>`_ as the 
underlying data object around which ATMOS wraps an interface. This provides 
some solid advantages:

- All data is attached (and detached) from a xarray.Dataset structure that
  saves all non-scalar quantities as an xarray.DataArray (basically a numpy 
  array with a powerful wrapper). Since xarray.Dataset can add and remove 
  associated DataArrays on the fly, there is no need to declare everything 
  that will be associated with an instance of Assembly in the source code 
  itself.

- When saving the data, one can rely on xarray to save the data
  as NetCDF files. These binary files can later be treated with most other 
  popular programming languages as well, avoiding language lock-in while 
  retaining speed of a binary format. This may be useful if greater speed 
  (than what is provided by Python) is desired in future.

- Because xarray is inspired by pandas and the underlying data is numpy, we
  have two interfaces (numpy and xarray (where interfacing is similar to pandas)) 
  for any data. A third simpler interface, suited for the kinds of operations
  done on atomistic systems, is provided by ATMOS. Having three interfaces to 
  the same data makes handling it super user-friendly/powerful.

- A lot of operations (such as merging datasets etc. and vectorized operations)
  can be off-loaded to xarray (and numpy) leaving ATMOS free to be a good 
  wrapper.

**Etymology**: The name is an intentional misspelling of the word 'atoms'. 
The idea is to communicate that mistakes are a feature of good science, not a
bug. Also, the word `atmos` stands for a gaseous envelope and ATMOS is just 
that - a light-weigt wrapper around xarray (and therefore numpy). It is also a 
good acronym for all that ATMOS is concerned about at the moment of its birth: 
Atoms, Tips and Molecules over Surfaces.

Why not use ASE?
****************

The developers have struggled with the idea if ATMOS is really needed because 
there exists a wonderful python package called `ASE <https://wiki.fysik.dtu.dk/ase>`_ 
that does a lot of similar (and more) stuff. However there were two reasons 
that ASE was not opted for. 

- ASE is also not extensible in terms of associated quantities -- e.g. each atom 
  can have just kind of charge. The same is true for forces etc. This is 
  limiting for the same reasons as BaseCode.

- ASE database cannot store arbitrary or voxeldata (such as cube file data)
  within itself. This makes it difficult to implement a 
  *one calculation* in one *storage file* solution with ASE.

Still, the developers of ATMOS are planning to make some sort of easy 
interfacing with ASE, especially to make use of ASE's excellent geometry 
creation/handling features.

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

"""
File to store settings. Feel free to modify as per needs.
"""

constrained_vector_charge_moment_error = False
""" To print (True), or not to print (False)
an error message when reading FHIaims geometry.in / geometry.in.next_step 
and encountering keywords "initial_charge", "initial_moment" 
or "constrain_relaxation" on a "lattice_vector".
"""

big_file_MBs_to_read = 128
"""Number of bytes to read when dealing with a big "cube" file."""

parallel_cube_write = True
"""Should ATMOS try and do parallel write-out for "cube" files."""

el_extra = [['8',   'O2', 'Oxygen2',      'FF0D0D'],  # Additional species should only appear after this point...
            ['7',   'N2', 'Nitrogen2',    '3050F8'],  # They must be named in Title case
            ['6',   'C2', 'Carbon2',      '909090'],  # Atomic numbers are purposefully stored as strings
            ['29',  'X1', 'Copper1',      'E68209'],  # so that for np.dtype of the list in the end is "str"
            ['29',  'X2', 'Copper2',      '5C3302'],
            ['1',   'H2', 'Hydrogen2',    'FFFFFF']]
""""New species can be added here."""

grimme_d3_path = "/home/physsh/Data.links/Data.secure/Gits.d/dftd3.d/dftd3" # store as pathlib.Path compatible string
"""Declare path of Grimme's D3 binary here."""
grimme_d3_arguments = "-func pbe -zero" # store as a string
"""Declare the arguments for Grimme's D3 binary here."""

# Post processing of variables set above - do not touch!
grimme_d3_vars = {"path" : grimme_d3_path,
                  "args" : grimme_d3_arguments}
"""
File to store various constants that are useful.
Changes can hit reproducibility.
"""
# Change at your own risk.

# Conversion values, generally from CODATA, https://codata.org/

# Energy
hartree2eV     = 27.211386245988
eV2hartree     = 1/hartree2eV
eV2joule       = 1.602176634e-19

# Distance
bohr2nm        = 0.052917721067
nm2bohr        = 1/bohr2nm
bohr2angstroem = 0.52917721067
angstroem2bohr = 1/bohr2angstroem
bohr2pm        = 52.917721067
pm2bohr        = 1/bohr2pm

angstroem2nm   = 0.1
nm2angstroem   = 10
angstroem2m    = 1e-10
m2angstroem    = 1e10
m2nm           = 1e-9
nm2m           = 1e9

# Force
eV_angstroem2nN = eV2joule / (angstroem2m*m2nm)
hartree_bohr2eV_angstroem = hartree2eV / bohr2angstroem



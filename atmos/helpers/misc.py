import uuid, numpy as np
from typing import List, Optional

def is_valid_uuid(value:str) -> bool:
    """Verify if a string constitutes a valid uuid.

    See `uuid <https://docs.python.org/3/library/uuid.html>`_ for more.

    Parameters
    ----------
    value : str
        String to verify.

    Returns
    -------
    bool
        True if valid uuid, else False.
    """
    try:
        uuid.UUID(value)
        return True
    except ValueError:
        return False

def is_signed_digit(value:str) -> bool:
    """To determine if a string is numerical.
    
    Same as str.isdigit() but takes care of sign (+/-) as well.
    Please remember that str.isdigit() is meant for int
    expressed as str. It will return False for strings 
    which have the decimal point / exponential e.

    Parameters
    ----------
    value : str
        Value that needs to be checked.

    Returns
    -------
    bool
        True if is digit, False if not.
    """
    if not isinstance(value, str):
        raise TypeError(f"Invalid value type: {type(value)}, expected: str.")
    value = value.strip()
    if value[0] in "+-":
        value = value[1:].lstrip(" ")
    return value.isdigit()

def lol_2_np(value:List[List[float]]) -> np.ndarray:
    """Make a numpy array out of list of lists where they can be unequal length.

    Fills np.nan to compensate for absent values.

    Parameters
    ----------
    value : List[List[float]]
        List of list, containing floats.

    Returns
    -------
    np.ndarray
        Numpy array of the required type.
    """
    length = max(map(len, value))
    arr    = np.array([i+([np.nan]*(length-len(i))) for i in value])
    return arr

def boolean_as_str_input(value:str) -> Optional[bool]:
    """Add some resiliency when understanding user input.

    Parameters
    ----------
    value : str
        Ideally, a string that maps to True or False depending on common English
        language usage

    Returns
    -------
    Optional[bool]
        True or False depending on interpretation of value. None if interpretation fails.
    """
    if value in ["True", "true", "T", "t", "Yes", "yes", "Y", "y"]:
        return True
    elif value in ["False", "false", "F", "f", "No", "no", "N", "n"]:
        return False
    if isinstance(value, bool):
        return value
    else:
        return None # return None if not able to comprehend or if value is some unexpected dtype
    
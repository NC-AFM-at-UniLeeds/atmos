from pathlib import Path
import atmos.assembly as atmos

def poscar_saver(filepath:Path, assembly:atmos.Assembly, fractional:bool = False, fluff:float = 10.0) -> None:
    """To write out a VASP POSCAR file.

    Parameters
    ----------
    filepath : Path
        Path of the VASP POSCAR file to be written.
    assembly : atmos.Assembly
        *assembly* to be written out.
    fractional : bool, optional
        If the file should be written out in fractional coordinate system. By default False.
    fluff : float, optional
        The writer supports adding a cell to a cell-less geometry by adding 
        an empty space on all sides. The empty space separates periodic images 
        of the atoms in all directions by <fluff> Å. The function receieves 
        the value of <fluff>. By default 10.0.
        Adding this fluff is not compatible with fractional coordinates as 
        a safety net.

    Raises
    ------
    ValueError
        If file is asked in absence of "positions" or fractional coordinates 
        are asked in absence of "dimensions".
    """
    if not assembly._has_quantity("positions"):
        raise ValueError("Can't write a VASP POSCAR without \"positions\".")
    if "unit" in assembly["positions"].attrs:
        assert assembly["positions"].attrs["unit"] == "angstroem", "POSCAR units must be angstroem."
    assembly = assembly.copy(copy_essentials=True) # a copy because we will modify it
    content = str(assembly)
    content += "\n1\n" # Line 2 : Scaling factor; always 1
    if assembly.dimensions is None:
        if fractional:
            raise ValueError("Cannot define a fractional coordinate system in absence of defined lattice vectors.")
        else:
            print(f"Lattice vectors for POSCAR being defined by adding {fluff} Å on each side of the system.")
            assembly << "translate_into_positives"
            content += f'{assembly["positions x"].data.max()+fluff: 15.10f} {0.: 15.10f} {0.: 15.10f}\n'
            content += f'{0.: 15.10f} {assembly["positions y"].data.max()+fluff: 15.10f} {0.: 15.10f}\n'
            content += f'{0.: 15.10f} {0.: 15.10f} {assembly["positions z"].data.max()+fluff: 15.10f}\n'
    else:
        if fractional: # since user supplies fractional as an argument, we assume they know what they are doing
            assembly << "fractional"
        else:
            if "coord_system" in assembly["positions"].attrs and assembly["positions"].attrs["coord_system"] == "fractional":
                assembly << "cartesian"
        for latt_vec in assembly.dimensions:
            content += f"{latt_vec[0]: 15.10f} {latt_vec[1]: 15.10f} {latt_vec[2]: 15.10f}\n" # Line 3-5 : Lattice vectors
    fixations_present = assembly._has_quantity("fixations")
    positions = assembly['positions'].data
    fixations = assembly['fixations'].data if fixations_present else None
    symbols   = assembly.atoms["symbols"]
    char_format = lambda x : 'T' if x else 'F'
    species_string    = [symbols[0]]  # start accumulating all the species
    counter_string    = [0]           # and their count
    content2          = ""            # second half of file's content
    content2 += "Selective dynamics\n" if fixations_present else "" # include this only if selective dynamics are required
    content2 += "Direct\n" if fractional else "Cartesian\n"             # choose between fractional and cartesian coordinate system
    for idx in range(len(assembly)):
        content2 += f"{positions[idx][0]: 15.10f} {positions[idx][1]: 15.10f} {positions[idx][2]: 15.10f}"
        if fixations_present:
            content2 += f"    {char_format(fixations[idx][0])}  {char_format(fixations[idx][1])}  {char_format(fixations[idx][2])}\n"
        else:
            content2 += "\n"
        if symbols[idx] != species_string[-1]:
            species_string.append(symbols[idx])
            counter_string.append(0)
        counter_string[-1] += 1
    species_string = " ".join(species_string)+"\n"
    counter_string = " ".join([str(_) for _ in counter_string])+"\n"
    file_content   = content + species_string + counter_string + content2
    with open(filepath, "w") as stream:
        stream.write(file_content)
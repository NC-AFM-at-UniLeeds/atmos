import numpy as np
from pathlib import Path
from typing import Optional, Union
from contextlib import suppress
from atmos.helpers.settings import constrained_vector_charge_moment_error
from atmos.helpers.misc import is_valid_uuid as _is_valid_uuid
from atmos.helpers.misc import lol_2_np
import atmos.assembly as atmos

class FHIaims_Assembly(atmos.Assembly):    
    """Inherits Assembly and adds FHIaims specific functionalities to it.

    Parameters
    ----------
    Assembly : atmos.Assembly
        The parent class: Assembly.

    Returns
    -------
    FHIaims_Assembly
        The child class: Assembly with FHIaims specific features.
    """
    @property
    def uuid(self) -> Optional[str]:
        """Attribute to get uuid(s) as read from FHIaims file.

        Returns
        -------
        Optional[str]
            uuid as read from an output file. uuid (or uuids if multiple found) 
            from geometry file. (Concatenated geometry files can have multiple 
            uuids in them.) Return None if no uuid is stored.
        """
        if self._has_quantity("uuid") == "scalar":
            return self.data.attrs["uuid"]
        if self._has_quantity("geo_uuid") == "scalar":
            return self.data.attrs["geo_uuid"]
        if self._has_quantity("geo_uuids") == "scalar":
            return self.data.attrs["geo_uuids"]
        return None

    @property
    def energy(self) -> Optional[float]:
        """Attribute to get energy as read from FHIaims file.

        Returns
        -------
        Optional[str]
            energy as read from an output file. Whether energy at T -> 0K is used
            or not is determined based on periodicity of assembly and number of 
            metal atoms. Can be overwritten by modifying the scalar "energy".
            Return None if no such scalar is found.
        """
        if self._has_quantity("energy") == "scalar":
            return self.data.attrs["energy"]
        return None
    
    def attach_cube(self, cube_name:Union[str, Path] = None) -> None:
        """Attach cubes associated with an output file to it.

        Parameters
        ----------
        cube_name : Union[str, Path], optional
            Name/path of the cube file to attach.
            If None, tries and picks up all filenames as were read in original
            output.aims file.

        Raises
        ------
        TypeError
            If input argument is not str or Path.
        FileNotFoundError
            If the required cube file is not found.
        """
        cubes = {}    
        if cube_name is None:
            if "cubes" in self.data.attrs["analysis"]:
                cubes = self.data.attrs["analysis"]["cubes"]
                filenames = cubes.keys()
            else:
                print("No associated cube filenames found.")
        else:
            if not isinstance(cube_name, (str, Path)):
                raise TypeError("attach_cube expects str or Path as input.")
            else:
                filenames = [cube_name]
        for filename in filenames:
            filepath = Path(filename)
            found = False
            if filepath.is_file():
                found = True
            elif "folderpath" in self.data.attrs["information"]:
                filepath = Path(self.data.attrs["information"]["folderpath"]) / filepath
                if filepath.is_file():
                    print(f"{filename} file found at its old path: {filepath}. Using it.")
                    found = True
            if found:
                quan_name = filename.lower().replace(".cube","").replace(".cub","").replace(".","_").replace("-","_")
                self.attach("voxeldat", quan_name, filepath)
                with suppress(KeyError):
                    self[quan_name].attrs["type"] = self.data.attrs["analysis"]["cubes"][filename]["type"]
                    self[quan_name].attrs["aims_cube_unit"] = self.data.attrs["analysis"]["cubes"][filename]["unit"]
            else:
                raise FileNotFoundError(f"Can't find {filename}.")

def _geo_data_parser(lines:list[str]) -> FHIaims_Assembly:
    """To parse lines as supplied from :func:`geofile_parser` or
    :func:`outfile_parser`. See :func:`geofile_parser` for details.

    Parameters
    ----------
    lines : list[str]
        Lines containing geometry data.

    Returns
    -------
    FHIaims_Assembly
        *assembly* holding the parsed data.

    Raises
    ------
    ValueError
        If data read is incorrect or incomplete e.g. number of atoms is 
        not same as number of positions read.
    """
    pbc        = 0
    numAtoms   = 0
    atoms      = []
    dimensions = []
    positions  = []
    fixations  = []
    ext_forces = []
    atom_comments = []
    last_latvec   = False
    atom_frac     = []
    uuid          = []
    vacuum_level  = None
    for line in lines:
        this_line = line.strip().split()
        if not len(this_line):
            continue # ignore empty lines
        if this_line[0].startswith("#"):
            this_line[0] = this_line[0][1:] # remove the commenting `#` in case uuid is immediately after the `#`
            for substring in this_line:
                if len(substring) == 36: # len(uuid) === 36
                    if _is_valid_uuid(substring):
                        uuid.append(substring)
            continue # ignore commented lines other than to find uuids
        this_line = this_line
        if this_line[0] == "set_vacuum_level":
            vacuum_level = float(this_line[1])
        if this_line[0] == "lattice_vector":
            pbc += 1
            dimensions.append(this_line[1:4])
        if constrained_vector_charge_moment_error:
            if last_latvec and "constrain_relaxation" in line:
                print("Constrained relaxation found for a lattice vector. This feature is currently not supported.\n"+
                      "Be careful that the information will also be removed from any write-outs.") # TODO: Move to UI
            if this_line[0] in ["initial_charge","initial_moment"]:
                print("Initial charge/moment found. This feature is currently not supported.\n"+
                      "Be careful that the information will also be removed from any write-outs.") # TODO: Move to UI
            last_latvec = (this_line[0] == "lattice_vector") # leave a value for when evaluating next line
        if this_line[0].startswith("atom"):
            atom_frac.append(this_line[0].startswith("atom_frac")) # make a note if atom_frac i.e. fractional coordinate are provided
            numAtoms += 1
            atoms.append(this_line[4])
            positions.append(this_line[1:4])
            fixations.append([False, False, False])
            ext_forces.append([np.nan, np.nan, np.nan])
            atom_comments.append("")
            if len(this_line) > 5:
                atom_comments[-1] = " ".join(this_line[5:])
        if this_line[0] == "constrain_relaxation" and not last_latvec:
            if "true" in this_line[1].lower():
                fixations[-1] = [True, True, True]
            if "x" in this_line[1]:
                fixations[-1][0] = True
            if "y" in this_line[1]:
                fixations[-1][1] = True
            if "z" in this_line[1]:
                fixations[-1][2] = True
        if this_line[0] == "external_force":
            ext_forces[-1] = this_line[1:4]
    if pbc == 3:
        pass
    elif pbc == 0:
        dimensions = None
    else:
        print("Invalid number of lattice_vectors were read. Removing all data related to Periodic Boundary Conditions.") # TODO: Move to UI
        dimensions = None
    atoms      = np.array(atoms, dtype=str)
    positions  = np.array(positions, dtype=float)
    if len(atoms) != len(positions):
        raise ValueError(f"Number of atoms: {len(atoms)} != number of coordinates found {len(positions)}.")
    fixations  = np.array(fixations, dtype=bool)
    ext_forces = np.array(ext_forces, dtype=float)
    atom_comments = np.array(atom_comments, dtype=str)
    assembly = FHIaims_Assembly(numAtoms)
    assembly["atoms"] = atoms
    if dimensions is not None:
        dimensions = np.array(dimensions)
        assembly.attach("cell", "dimensions", dimensions, dtype=float)
        if any(atom_frac):
            for atom, is_frac in enumerate(atom_frac):
                if is_frac:
                    positions[atom] = positions[atom] @ dimensions # convert fractional to cartesian
    elif any(atom_frac):
        raise ValueError("Can't have fractional co-ordinates in absence of lattice vectors.") # TODO: Move to UI
    else:
        pass # do not set dimensions i.e. periodic boundary condition is False
    assembly["positions"] = positions
    assembly["positions"].attrs["coord_system"] = "cartesian"
    if all(atom_frac) and assembly.dimensions is not None:
        assembly << "fractional" # convert back to fractional if all atoms were using atom_frac
        # assembly["positions"].attrs["coord_system"] = "fractional" is done by the << operator
    assembly["positions"].attrs["unit"] = "angstroem"
    if uuid:
        if len(uuid) == 1:
            assembly.attach("scalar","geo_uuid", uuid[0])
        else:
            assembly.attach("scalar","geo_uuids", ", ".join(uuid))
    if not vacuum_level is None:
        assembly.attach("scalar","vacuum_level", vacuum_level)
    if not np.isnan(fixations).all():
        assembly.attach("n_vector", "fixations", fixations, dtype=bool)
    if not np.isnan(ext_forces).all():
        assembly.attach("n_vector", "forces~external", ext_forces, dtype=float)
    if np.char.str_len(atom_comments).sum():
        assembly.attach("n_scalar", "atom_comments", atom_comments, dtype=str)
    return assembly

def geofile_parser(filepath:Path) -> FHIaims_Assembly:
    """Reads a geometry.in / geometry.in.next_step file.

    Data read includes atomic species, positions, contraints (if any), 
    external forces (if any) and comments on lines that begin with string 
    `atom` or `atom_frac`.
    Attached data takes the form:
    
        - assembly["atoms"]           of kind "atoms"
        - assembly["positions"]       of kind "n_vector", always in Cartesian coordinates
        - assembly["dimensions"]      of kind "cell", only if lattice_vector are read
        - assembly["geo_uuid"]        or
        - assembly["geo_uuid"]        of kind "scalar", if one or more uuids are read
        - assembly["fixations"]       of kind "n_vector", dtype = bool, if read
        - assembly["forces~external"] of kind "n_vector", if read
        - assembly["atom_comments"]   of kind "n_scalar", dtype = str, if read
        - assembly["vacuum_level"]    of kind "scalar", if read

    Parameters
    ----------
    filepath : Path
        Path to the file that has to be read.

    Returns
    -------
    FHIaims_Assembly
        *assembly* with data from the geometry.in / geometry.in.next_step.
    """
    with open(filepath, 'r') as stream:
        return _geo_data_parser(stream.readlines())

def geofile_saver(filepath:Path, assembly:Union[atmos.Assembly, FHIaims_Assembly], external_forces:str = None, fractional:bool = False) -> None:
    """To write out a geometry.in or geometry.in.next_step (FHI-aims geometry) file.

    Parameters
    ----------
    filepath : Path
        Path of the FHI-aims geometry file to be written.
    assembly : Union[atmos.Assembly, FHIaims_Assembly]
        *assembly* to be written out.
    external_forces : str, optional
        If not None, the 'quantity' name that would be written out as `external_forces`. 
        Maybe be useful to write out forces from D3 calculator in the geometry file.
        By default None.
    fractional : bool, optional
        If the file should be written out in fractional coordinate system. By default False.

    Raises
    ------
    ValueError
        If file is asked in absence of "positions" or fractional coordinates 
        are asked in absence of "dimensions" or the quantity name sent as `external_forces`
        is not found.
    TypeError
        If assembly[`external_forces`] is not an "n_vector".
    """
    if not assembly._has_quantity("positions"):
        raise ValueError("Can't write an FHI-aims file without \"positions\".")
    output = f"# {assembly}\n" # start with a comment
    if assembly.dimensions is not None:
        for latt_vec in assembly.dimensions:
            output += f"lattice_vector {latt_vec[0]: 15.10f} {latt_vec[1]: 15.10f} {latt_vec[2]: 15.10f}\n"
    if assembly._has_quantity("vacuum_level"):
        output += f"set_vacuum_level {assembly['vacuum_level']: 15.10f}\n"
    elif assembly._has_quantity("levels"):
        if isinstance(assembly["levels"], dict):
            try:
                output += f"set_vacuum_level {assembly['levels']['vacuum_level']: 15.10f}\n"
            except KeyError:
                pass
    symbols = assembly.atoms["symbols"]
    positions = assembly["positions"].data
    assert assembly["positions"].attrs["unit"] == "angstroem", "geometry.in positions must be defined in angstroem"
    fixations = assembly["fixations"] if assembly._has_quantity("fixations") else None
    if external_forces is not None:
        if (kind := assembly._has_quantity(external_forces)) and kind == "n_vector":
            external_forces = assembly[external_forces].data
            if np.isnan(external_forces).any():
                print("Warning: Replacing nan forces with 0.0.")
                np.nan_to_num(external_forces, copy=False)
        elif kind:
            raise TypeError(f"Can't write {external_forces} ({kind}). It must be of kind 'n_vector'.")
        else:
            raise ValueError(f"Quantity called {external_forces} is not defined.")
    tagged_fractional    = "coord_system" in assembly["positions"].attrs and assembly["positions"].attrs["coord_system"] == "fractional"
    tagged_cartesian     = "coord_system" in assembly["positions"].attrs and assembly["positions"].attrs["coord_system"] == "cartesian"
    confirmed_fractional = bool(np.all(assembly["positions"] <= 1)) and (assembly.dimensions is not None)
    if tagged_fractional:
        assert confirmed_fractional, "It seems that your coordinates are marked as fractional but they are not so."
    if fractional:
        atom_str = "atom_frac"
        if assembly.dimensions is None:
            raise ValueError("Cannot write a file with fractional coordinate system in absence of defined lattice vectors.")
        if confirmed_fractional:
            if not tagged_fractional:
                print("CAUTION: It seems that the coordinates are in fractional notation. Will use them as such.")
        else:
            print("CAUTION: It seems that the coordinates are in cartesian notation. Will convert before writing.")
            assembly_copy = assembly.copy(copy_essentials=True)
            assembly_copy << "fractional"
            positions = assembly_copy["positions"].data
            del assembly_copy
    else:
        atom_str = "atom"
        if tagged_cartesian:
            if confirmed_fractional:
                print(f"CAUTION: Your coordinates look as if they are fractional. You sure you they are Cartesian? Writing {filepath} anyway.")
        else:
            if confirmed_fractional:
                assembly_copy = assembly.copy(copy_essentials=True)
                assembly_copy << "cartesian"
                positions = assembly_copy["positions"].data
                del assembly_copy
    for idx in range(len(assembly)):
        output += f"{atom_str} {positions[idx][0]: 15.10f} {positions[idx][1]: 15.10f} {positions[idx][2]: 15.10f} {symbols[idx]:>4}\n"
        if fixations is not None:
            if fixations[idx].all():
                output     += "    constrain_relaxation    .true.\n"
            else:
                if fixations[idx][0]:
                    output += "    constrain_relaxation    x\n"
                if fixations[idx][1]:
                    output += "    constrain_relaxation    y\n"
                if fixations[idx][2]:
                    output += "    constrain_relaxation    z\n"
        if external_forces is not None:
            if external_forces[idx].any():
                output += "    external_force   {external_forces[idx][0]: 15.10f} {external_forces[idx][1]: 15.10f} {external_forces[idx][2]: 15.10f}\n"
    with open(filepath, 'w') as stream:
        stream.write(output)

def outfile_parser(filepath:Path) -> FHIaims_Assembly:
    """Read FHI-aims output file.

    Parameters
    ----------
    filepath : Path
        File to be read.

    Returns
    -------
    FHIaims_Assembly
        *assembly* with many associated data. Use *assembly*.info() to explore the data.
    """
    with open(filepath, 'r') as stream:
        scalars = {"information":{"success":False},
                   "stats":{},
                   "species":{},
                   "criteria":{},
                   "levels":{},
                   "energies":{},
                   "charge":{},
                   "analysis":{"kinds": set()},
                   "unit": {"criteria": {}, "levels": {}, "energies":{}, "charge":{}}}
        r_header, r_ctrl_verbatim, r_geo_verbatim, r_ctrl_info, r_geo_info, r_scf = True, False, False, False, False, False
        collect_energies, collect_evol, collect_mull, collect_cube, collect_stats = False, False, False, False, False
        fixations, cubes, positions, total_forces, residual_forces, cubes_unit = None, {}, [], [], [], None
        scf_result = {"evolution~delta_charge_density":     [],
                      "evolution~delta_sum_of_eigenvalues": [],
                      "evolution~delta_total_energy":       [],
                      "evolution~delta_forces":             [],
                      "evolution~iteration_time":           [],
                      "energy~sum_of_eigenvalues":          [],
                      "energy~xc_energy_correction":        [],
                      "energy~xc_potential_correction":     [],
                      "energy~free_atom_electrostatic":     [],
                      "energy~hartree_correction":          [],
                      "energy~vdw_correction":              [],
                      "energy~vdw_mdd_rs_csc_potential":    [],
                      "energy~entropy_correction":          [],
                      "energy~non_metal_total":             [],
                      "energy~metal_total":                 [],
                      "energy~free_electronic":             [],
                      "energy~kinetic":                     [],
                      "energy~electrostatic":               [],
                      "energy~correction_multipole_error":  [],
                      "energy~sum_of_eigenvalues_per_atom": [],
                      "energy~metal_total_per_atom":        [],
                      "energy~free_electronic_per_atom":    [],
        }
        scf_result_unit = {"evolution~delta_charge_density":     "electrons/bohr^3",
                           "evolution~delta_sum_of_eigenvalues": "eV",
                           "evolution~delta_total_energy":       "eV",
                           "evolution~delta_forces":             "eV/angstroem",
                           "evolution~iteration_time":           "s",
                           "energy~sum_of_eigenvalues":          "eV",
                           "energy~xc_energy_correction":        "eV",
                           "energy~xc_potential_correction":     "eV",
                           "energy~free_atom_electrostatic":     "eV",
                           "energy~hartree_correction":          "eV",
                           "energy~vdw_correction":              "eV",
                           "energy~vdw_mdd_rs_csc_potential":    "eV",
                           "energy~entropy_correction":          "eV",
                           "energy~non_metal_total":             "eV",
                           "energy~metal_total":                 "eV",
                           "energy~free_electronic":             "eV",
                           "energy~kinetic":                     "eV",
                           "energy~electrostatic":               "eV",
                           "energy~correction_multipole_error":  "eV",
                           "energy~sum_of_eigenvalues_per_atom": "eV",
                           "energy~metal_total_per_atom":        "eV",
                           "energy~free_electronic_per_atom":    "eV",
        }
        line_meaning = {"Sum of eigenvalues":             "energy~sum_of_eigenvalues",
                        "XC energy correction":           "energy~xc_energy_correction",
                        "XC potential correction":        "energy~xc_potential_correction",
                        "Free-atom electrostatic energy": "energy~free_atom_electrostatic",
                        "Hartree energy correction":      "energy~hartree_correction",
                        "vdW energy correction":          "energy~vdw_correction",
                        "MBD@rsSCS energy":               "energy~vdw_correction",
                        "MBD@rsSCS potential":            "energy~vdw_mdd_rs_csc_potential",
                        "Entropy correction":             "energy~entropy_correction",
                        "Total energy":                   "energy~non_metal_total",
                        "Total energy, T -> 0":           "energy~metal_total",
                        "Electronic free energy":         "energy~free_electronic",
                        "Kinetic energy":                 "energy~kinetic",
                        "Electrostatic energy":           "energy~electrostatic",
                        "error in Hartree potential":     "energy~correction_multipole_error",
                        "Sum of eigenvalues per atom":    "energy~sum_of_eigenvalues_per_atom",
                        "Total energy (T->0) per atom":   "energy~metal_total_per_atom",
                        "Electronic free energy per atom":"energy~free_electronic_per_atom",
        }
        for line in stream:
            line = line.strip()
            # Read data at the start of the file
            if r_header:
                if line.startswith("Date"):
                    line = line.split()
                    scalars["stats"]["start"] = {}
                    scalars["stats"]["start"]["date"] = {"YYYY":int(line[2][:4]), "MM":int(line[2][4:6]), "DD":int(line[2][6:8])}
                    scalars["stats"]["start"]["time"] = {"HH":int(line[5][:2]), "mm":int(line[5][2:4]), "ss": int(line[5][4:6])}
                    continue
                if line.startswith("aims_uuid"):
                    value = line.split()[-1]
                    if _is_valid_uuid(value):
                        scalars["uuid"] = value
                    continue
                if line.startswith("FHI-aims version"):
                    scalars["information"]["aims_version"] = line.split()[-1]
                    continue
                if line.endswith("parallel tasks.") and line.startswith("Using"):
                    parallel_tasks = int(line.split()[-3])
                    nodes = set()
                    for i in range(parallel_tasks):
                        line = next(stream).strip()
                        if line.endswith("reporting."):
                            nodes.add(line.split()[-2])
                        else:
                            nodes = set() # render empty because incomplete info.
                            break
                    scalars["information"]["parallel_tasks"] = parallel_tasks
                    if nodes:
                        scalars["information"]["nodes"] = " ".join(nodes)
                    del parallel_tasks, nodes
                    continue
                if line.startswith("Parsing control.in (first pass over file"):
                    r_header = False
                    r_ctrl_verbatim = True
                    continue
                if line.startswith("Parsing geometry.in (first pass over file"):
                    r_header = False                    
                    r_geo_verbatim = True
                    continue
                if line.startswith("| Number of species"):
                    scalars["information"]["num_species"] = int(line.split()[-1])
                    continue
                if line.startswith("| Number of atoms"):
                    num_atoms = int(line.split()[-1])
                    continue
                if line.startswith("| Number of spin channels"):
                    scalars["information"]["num_spin"] = int(line.split()[-1])
                    continue
                if line.startswith("| Cube type output requested"):
                    scalars["stats"]["num_cubes"] = int(line.split()[-1])
                    continue
                if line.startswith("Reading file control.in."):
                    assembly = FHIaims_Assembly(num_atoms)
                    if fixations is not None:
                        assembly.attach("n_vector", "fixations", fixations, dtype=bool)
                    r_header = False
                    r_ctrl_info = True
            # Only if verbatim control.in has been output: use it to determine the species kinds: "light", "intermediate", "tight" or "really_tight"
            if r_ctrl_verbatim:
                line = line.split("#",maxsplit=1)[0].strip()
                if line.startswith("cube_content_unit"):
                    cubes_unit = line.split()[-1]
                    continue                
                if line.startswith("output cube"):
                    cube_name = f"unknown_cube_num_{len(cubes)+1}"
                    cubes[cube_name] = {"type": line.split()[-1]}
                if line.startswith("cube filename"):
                    cube_type = cubes[cube_name]["type"]
                    del cubes[cube_name]
                    cube_name = line.split()[-1]
                    cubes[cube_name] = {"type": cube_type}
                    if cubes_unit:
                        cubes[cube_name]["unit"] = cubes_unit
                    continue
                if line.startswith("#  Suggested"):
                    line = line.split()
                    sp_kind = line[2].strip('"')
                    sp = line[5]
                    if sp_kind in ["light", "intermediate", "tight", "really_tight"]:
                        scalars["species"][sp] = {"kind": sp_kind}
                    continue
                if line.startswith("Completed first pass over input file control.in"):
                    if cubes:
                        scalars["analysis"]["cubes"] = cubes
                    r_ctrl_verbatim = False
                    r_header = True
            # Only if verbatim geometry.in has been placed in output: use it to determine fixations and the vacuum level
            if r_geo_verbatim:
                geo_lines = []
                while not line.startswith("Completed first pass over input file geometry.in"):
                    line = next(stream).strip()
                    geo_lines.append(line)
                geo_assembly = _geo_data_parser(geo_lines)
                fixations = geo_assembly["fixations"].data
                if geo_assembly._has_quantity("vacuum_level"):
                    scalars["level"]["vacuum_level"] = geo_assembly["vacuum_level"]
                    scalars["unit"]["level"]["vacuum_level"] = "angstroem"
                r_geo_verbatim = False
                r_header = True
            # Start reading the control file's data
            # assembly must be initialized before this point
            if r_ctrl_info:
                if line.startswith("XC: "):
                    scalars["information"]["xc_functional"] = line.split()[2]
                    continue
                if line.startswith("Charge ="):
                    scalars["charge"]["ctrl_file"] = float(line.split()[2].rstrip(":"))
                    scalars["unit"]["charge"]["ctrl_file"] = "e" # elementary charge
                    continue
                if line.startswith("Convergence accuracy of total energy"):
                    scalars["criteria"]["total_energy"] = float(line.split()[-1])
                    scalars["unit"]["criteria"]["total_energy"] = "eV"
                    continue
                if line.startswith("Convergence accuracy of sum of eigenvalues"):
                    scalars["criteria"]["sum_of_eigenvalues"] = float(line.split()[-1])
                    scalars["unit"]["criteria"]["sum_of_eigenvalues"] = "eV"
                    continue
                if line.startswith("Convergence accuracy of potential jump"):
                    scalars["criteria"]["potential_jump"] = float(line.split()[-1])
                    scalars["unit"]["criteria"]["potential_jump"] = "eV"
                    continue
                if line.startswith("Found k-point grid"):
                    assembly.attach("vector", "k_point_grid", line.split()[-3:], dtype=int)
                    continue
                if line.startswith("K-point grid offset"):
                    assembly.attach("vector", "k_grid_offset", line.split()[-3:], dtype=float)
                    continue
                if line.startswith("Convergence accuracy of forces"):
                    scalars["criteria"]["forces"] = float(line.split()[-1])
                    scalars["unit"]["criteria"]["forces"] = "eV/angstroem"
                    continue
                if line.startswith("Convergence accuracy for geometry relaxation"):
                    scalars["criteria"]["relaxation"] = float(line.split()[-2])
                    scalars["unit"]["criteria"]["relaxation"] = "eV/angstroem"
                    continue
                if line.startswith("Reading configuration options for species"):
                    line = line.rstrip(".").strip()
                    sp = line.split()[-1]
                    if sp not in scalars["species"].keys():
                        scalars["species"][sp] = {}
                    for i in range(10): # check next 10 lines to find nuclear charge i.e. atomic number
                        line = next(stream).strip()
                        if line.startswith("| Found nuclear charge"):
                            scalars["species"][sp]["nuclear_charge"] = float(line.split()[-1])
                            break
                    for i in range(10): # check next 10 lines to TS-vdW parameters
                        line = next(stream).strip()
                        if line.startswith("| Found explicit parameters for TS-vdw correction"):
                            scalars["species"][sp]["vdw_param_TS"] = {}
                            for j in range(3):
                                line = next(stream).strip().split()
                                scalars["species"][sp]["vdw_param_TS"][line[1]] = float(line[-1])
                            break
                    assert sp in assembly.data._elements.loc[:,"sym"], f"Could not determine {sp} as a valid species. See atmos.helpers.variables for more."
                    continue
                if line.startswith("* Notice: The s.c.f. convergence criterion sc_accuracy_rho was not provided."):
                    for i in range(10): # check next 10 lines to find sc_accuracy_rho
                        line = next(stream).strip()
                        if "sc_accuracy_rho =" in line:
                            scalars["criteria"]["charge_density"] = float(line.split()[-2])
                            scalars["unit"]["criteria"]["charge_density"] = "electrons/bohr^3"
                            break
                    continue
                if line.startswith("Reading geometry description geometry.in."):
                    r_ctrl_info = False
                    r_geo_info = True
                    continue
            # start reading input geometry
            if r_geo_info:
                if line.startswith("Found relaxation constraint for atom") and fixations is None:
                    if not assembly._has_quantity("fixations"):
                        assembly.attach("n_vector", "fixations", None, dtype=bool)
                    atom_id = int(line.split()[5].rstrip(":")) - 1
                    if line.endswith("All coordinates fixed."):
                        assembly.data["fixations"][atom_id] = [True, True, True]
                    continue
                if line.startswith("The structure contains"):
                    assert int(line.split()[3]) == num_atoms, f"Number of atoms in geometry incorrect in file: {filepath}"
                    scalars["information"]["num_elec"] = float(line.split()[-2])
                    continue
                if line.startswith("| No unit cell requested."):
                    assert assembly.dimensions is None, f"No lattice vectors found in file: {filepath}\nwhereas assembly has {assembly.dimensions}"
                    continue
                if line.startswith("| Unit cell:"):
                    dimensions = []
                    for i in range(3):
                        line = next(stream).strip().lstrip("|").split()
                        dimensions.append(line)
                    assembly.attach("cell","dimensions",dimensions,float)
                    del dimensions
                    continue
                if line.startswith("| Atomic structure:"):
                    line = next(stream)
                    for atom_id in range(num_atoms):
                        line = next(stream).strip().split()
                        assembly[atom_id] = line[-4]
                        assembly.data["positions"][atom_id] = line[-3:]
                        assembly["positions"].attrs["unit"] = "angstroem"
                    positions.append(assembly["positions"].data)
                    continue
                if line.startswith("| Number of Kohn-Sham states (occupied + empty):"):
                    num_ks_states = int(line.split()[-1])
                    continue
                if line.startswith("Preparing all fixed parts of the calculation."):
                    r_geo_info = False
                    continue            
            # Follow every SCF - cycle
            if line.startswith("Begin self-consistency"):
                if line.endswith("nitialization."):
                    for k in scf_result.keys():
                        scf_result[k].append([])
                ks_states = []
                r_scf = True
                continue
            if r_scf:
                if line.startswith("| Chemical potential (Fermi level):"):
                    scalars["levels"]["chemical_potential"] = float(line.split()[-2])
                    continue
                if line.startswith("Writing Kohn-Sham eigenvalues."):
                    for i in range(10): # check next 10 lines to find header of the eignevalue table
                        line = next(stream).strip().split()
                        if "State" in line and "Occupation" in line:
                            break
                    for i in range(num_ks_states):
                        line = next(stream).strip().split()
                        ks_states.append([int(line[0]), float(line[1]), float(line[3])]) # taking energy in eV
                    continue
                if line.startswith("Highest occupied state (VBM) at"):
                    line = line.rstrip("(relative to internal zero)")
                    scalars["levels"]["highest_occupied"] = {"energy": float(line.split()[-2])}
                    for i in range(10): # check next 10 lines to find occupation
                        line = next(stream).strip()
                        if line.startswith("| Occupation number:"):
                            scalars["levels"]["highest_occupied"]["occupation"] = float(line.split()[-1])
                            break
                    continue
                if line.startswith("Lowest unoccupied state (CBM) at"):
                    line = line.rstrip("(relative to internal zero)")
                    scalars["levels"]["lowest_unoccupied"] = {"energy": float(line.split()[-2])}
                    for i in range(10): # check next 10 lines to find occupation
                        line = next(stream).strip()
                        if line.startswith("| Occupation number:"):
                            scalars["levels"]["lowest_unoccupied"]["occupation"] = float(line.split()[-1])
                            break
                    continue
                if line.startswith("Overall HOMO-LUMO gap:"):
                    scalars["levels"]["bandgap"] =  float(line.split()[-2])
                    continue
                if line.startswith("ESTIMATED overall HOMO-LUMO gap:"):
                    scalars["levels"]["bandgap"] = float(line.split()[4])
                    continue
                if line.startswith("| This appears to be an direct band gap."):
                    scalars["levels"]["bandgap_type"] = "direct"
                    continue
                if line.startswith("| This appears to be an indirect band gap."):
                    scalars["levels"]["bandgap_type"] = "indirect"
                    continue
                if line.startswith('| Work function ("upper" slab surface)'):
                    scalars["levels"]["work_function"] = {"upper_slab_surface": float(line.split()[-2])}
                    continue
                if line.startswith('| Work function ("lower" slab surface)'):
                    scalars["levels"]["work_function"]["lower_slab_surface"] = float(line.split()[-2])
                    continue
                if line.startswith("| VBM (reference: upper vacuum level)"):
                    scalars["levels"]["VBM"] = float(line.split()[-2])
                    continue
                if line.startswith("| CBM (reference: upper vacuum level)"):
                    scalars["levels"]["CBM"] = float(line.split()[-2])
                    continue
                if line.startswith("Libmbd: VdW method:"):
                    if line.split()[-1] == "mbd-rsscs":
                        scalars["analysis"]["kinds"].add("MBD-rsscs")
                if line.startswith("Total energy components:"):
                    collect_energies = True
                    continue
                if collect_energies:
                    line = line.split("<--", maxsplit=1)[0]        # get rid of comment about periodic metals
                    if (not line.startswith("|")) or line.startswith("| ------------------") or line.startswith("| Energy correction for multipole"):
                        continue
                    value = float(line.split()[-2])
                    line = line.split(":", maxsplit=1)[0].strip() # get rid of numerical part
                    line = line.lstrip("|").strip()               # get rid of the pipe part
                    if line == "MBD@rsSCS energy":
                        scalars["analysis"]["kinds"].add("MBD-rsscs")
                    line = line_meaning[line]
                    scf_result[line][-1] = value
                    if line == "energy~free_electronic_per_atom":
                        collect_energies = False
                if line.startswith("atomic forces [eV/Ang]"):
                    hellmann_feynman_forces, ionic_forces, multipole_forces, pulay_gga_forces, forces_vdw, total_forces = [], [], [], [], [], []
                    while len(total_forces) != num_atoms:
                        line = next(stream).strip()
                        if line.startswith("Hellmann-Feynman"):
                            hellmann_feynman_forces.append(np.array(line.split()[-3:], dtype=float))
                            continue
                        if line.startswith("Ionic forces"):
                            ionic_forces.append(np.array(line.split()[-3:], dtype=float))
                            continue
                        if line.startswith("Multipole"):
                            multipole_forces.append(np.array(line.split()[-3:], dtype=float))
                            continue
                        if line.startswith("Pulay + GGA"):
                            pulay_gga_forces.append(np.array(line.split()[-3:], dtype=float))
                            continue
                        if line.startswith("Van der Waals"):
                            forces_vdw.append(np.array(line.split()[-3:], dtype=float))
                            continue
                        if line.startswith("Total forces"):
                            total_forces.append(np.array(line.split()[-3:], dtype=float))
                            continue                                                
                        continue
                if line.startswith("Maximum force component is"):
                    residual_forces.append(float(line.split()[-2]))
                    continue
                if line.startswith("Updated atomic structure:"):
                    positions.append(np.full(assembly["positions"].shape, fill_value=np.nan))
                    atom_id = 0
                    while atom_id < num_atoms:
                        line = next(stream).strip().split()
                        if line and line[0].startswith("atom"):
                            positions[-1][atom_id] = np.array(line[1:4],dtype=float)
                            atom_id += 1
                    continue
                if line.startswith("Self-consistency convergence accuracy:"):
                    collect_evol = True
                    for k in scf_result.keys():
                        if k.startswith("evolution"):
                            scf_result[k][-1].append(np.nan) # make it nan so that it remains nan if not replaced later
                    continue
                if collect_evol:
                    if line.startswith("| Change of charge density"):
                        scf_result["evolution~delta_charge_density"][-1][-1] = float(line.split()[-1])
                        continue
                    if line.startswith("| Change of sum of eigenvalues"):
                        scf_result["evolution~delta_sum_of_eigenvalues"][-1][-1] = float(line.split()[-2])
                        continue
                    if line.startswith("| Change of total energy"):
                        scf_result["evolution~delta_total_energy"][-1][-1] = float(line.split()[-2])
                        continue
                    if line.startswith("| Change of forces"):
                        scf_result["evolution~delta_forces"][-1][-1] = float(line.split()[-2])
                        continue
                    if line.startswith("End self-consistency iteration") or line.startswith("End scf initialization - timings"):
                        for i in range(10): # check next 10 lines to find time for iteration
                            line = next(stream).strip()
                            if line.startswith("| Time for this iteration") or line.startswith("| Time for scf. initialization"):
                                scf_result["evolution~iteration_time"][-1][-1] = float(line.split()[-2])
                                break
                        collect_evol = False
            if line.startswith("Performing Hirshfeld analysis of fragment charges and moments."):
                if "hirshfeld" in scalars["analysis"]["kinds"]:
                    pass
                else:
                    assembly.attach("n_scalar","hirshfeld~charge", unit="e") # elementary charge
                    assembly.attach("n_scalar","hirshfeld~volume")
                    assembly.attach("n_vector","hirshfeld~dipole_vector")
                    assembly.attach("n_scalar","hirshfeld~dipole_moment")
                    scalars["analysis"]["kinds"].add("hirshfeld")
                hirshfeld_charge, hirshfeld_volume, hirshfeld_dipole_vector, hirshfeld_dipole_moment= [], [], [], []
                syms = assembly.atoms["symbols"]
                for i in range(num_atoms): # for as many as atoms are there
                    for j in range(10): # skip 10 lines:
                        line = next(stream).strip()
                        if line.startswith("| Atom"):
                            assert syms[i] == line.split()[-1], f"Wrong tally between hirshfeld analysis atom {line.split()[-1]} and geometry atom {syms[i]}."
                            continue
                        if line.startswith("|   Hirshfeld charge"):
                            hirshfeld_charge.append(line.split()[-1])
                        if line.startswith("|   Hirshfeld volume"):
                            hirshfeld_volume.append(line.split()[-1])
                        if line.startswith("|   Hirshfeld dipole vector"):
                            hirshfeld_dipole_vector.append(line.split()[-3:])
                        if line.startswith("|   Hirshfeld dipole moment :"):
                            hirshfeld_dipole_moment.append(line.split()[-1])
                assembly["hirshfeld~charge"] = hirshfeld_charge
                assembly["hirshfeld~volume"] = hirshfeld_volume
                assembly["hirshfeld~dipole_vector"] = hirshfeld_dipole_vector
                assembly["hirshfeld~dipole_moment"] = hirshfeld_dipole_moment
                scalars["charge"]["hirshfeld"] = float(assembly["hirshfeld~charge"].sum())
                scalars["unit"]["charge"]["hirshfeld"] = "e" # elementary charge
                continue
            if line.startswith("Starting Mulliken Analysis"):
                scalars["analysis"]["kinds"].add("mulliken")
                collect_mull = True
                continue
            if collect_mull:
                if not line.startswith("|"):
                    continue
                line = line.lstrip("|").strip().split()
                if line: # ignore empty lines
                    if line[0] == "atom":           
                        mulliken_charge = np.empty(num_atoms)
                        num_components = len(line)-2
                        mulliken_analysis = np.full((num_atoms, num_components), fill_value=np.nan)
                        for i in range(num_atoms):
                            line = next(stream).split()
                            mulliken_charge[i] = line[3]
                            mulliken_analysis[i][0] = line[1]
                            for j in range(1, num_components):
                                try:
                                    mulliken_analysis[i][j] = line[j+3]
                                except IndexError:
                                    pass
                        assembly.attach("n_scalar", "mulliken~charge", mulliken_charge, dtype="float", unit="e") # elementary charge
                        scalars["charge"]["mulliken"] = float(assembly["mulliken~charge"].sum())
                        scalars["unit"]["charge"]["mulliken"] = "e" # elementary charge
                        assembly.attach("arbit", "mulliken~analysis", mulliken_analysis, dtype="float")
                        assembly["mulliken~analysis"].attrs["order"] = ["atom_id","l = 0, 1, 2 ..."]
                    collect_mull = False
                continue              
            if line.startswith("The following cube files will be created") and len(cubes) != scalars["stats"]["num_cubes"]:
                cubes.clear()
                collect_cube = True
                continue
            if collect_cube:
                if line.startswith("cube filename"):
                    cubes[line.split()[-1]] = {}
                    if len(cubes) == scalars["stats"]["num_cubes"]:
                        collect_cube = False
                    continue
            if line.startswith("Leaving FHI-aims"):
                r_scf = False
                collect_stats = True
                continue
            if collect_stats:
                if line.startswith("Date"):
                    line = line.split()
                    scalars["stats"]["end"] = {}
                    scalars["stats"]["end"]["date"] = {"YYYY":int(line[2][:4]), "MM":int(line[2][4:6]), "DD":int(line[2][6:8])}
                    scalars["stats"]["end"]["time"] = {"HH":int(line[5][:2]), "mm":int(line[5][2:4]), "ss": int(line[5][4:6])}
                    continue
                if line.startswith("| Number of self-consistency cycles"):
                    scalars["stats"]["num_scf"] = int(line.split()[-1])
                    continue
                if line.startswith("| Number of SCF (re)initializations"):
                    scalars["stats"]["num_scf_init"] = int(line.split()[-1])
                    continue
                if line.startswith("| Number of relaxation steps"):
                    scalars["stats"]["num_relax_steps"] = int(line.split()[-1])
                    scalars["analysis"]["kinds"].add("geo_relax")
                    continue
                if line.startswith("| Total time"):
                    scalars["stats"]["total_time"] = float(line.split()[-2])
                    scalars["stats"]["time_parts"] = {}
                    while not line.startswith("Partial memory accounting:"):
                        line = next(stream).strip().lstrip("|").strip()
                        if line.endswith("s"):
                            line, value = line.split(":", maxsplit=1)
                            line = line.strip().replace("Total time for ","").replace("-","_").replace(" ","_").replace(".","_").replace("__","_").lower()
                            value = float(value.split()[-2])
                            scalars["stats"]["time_parts"][line] = value
                        continue
                    max_mem = 0
                    while not line.startswith('The "true" memory usage will be greater.'):
                        line = next(stream).strip().lstrip("|").strip()
                        if line.startswith("Maximum"):
                            if float(line.split()[1]) > max_mem:
                                max_mem = float(line.split()[1])
                                scalars["stats"]["max_mem"] = " ".join(line.split()[1:3])
                        continue
                if line.startswith("Have a nice day."):
                    scalars["information"]["success"] = True
                    collect_stats = False
                    continue
        if "chemical_potential" in scalars["levels"]:
            scalars["unit"]["levels"]["chemical_potential"] = "eV"
        if "highest_occupied" in scalars["levels"]:
            if "energy" in scalars["levels"]["highest_occupied"]:
                scalars["unit"]["levels"]["highest_occupied"] = {"energy":"eV"}
            if "occupation" in scalars["levels"]["highest_occupied"]:
                scalars["unit"]["levels"]["highest_occupied"]["occupation"] = "electrons"
        if "lowest_unoccupied" in scalars["levels"]:
            if "energy" in scalars["levels"]["lowest_unoccupied"]:
                scalars["unit"]["levels"]["lowest_unoccupied"] = {"energy":"eV"}
            if "occupation" in scalars["levels"]["lowest_unoccupied"]:
                scalars["unit"]["levels"]["lowest_unoccupied"]["occupation"] = "electrons"
        if "work_function" in scalars["levels"]:
            scalars["unit"]["levels"]["work_function"] = {"upper_slab_surface": "eV",
                                                          "lower_slab_surface": "eV"}
        if "VBM" in scalars["levels"]:
            scalars["unit"]["levels"]["VBM"] = "eV"
        if "CBM" in scalars["levels"]:
            scalars["unit"]["levels"]["CBM"] = "eV"
        if "bandgap" in scalars["levels"]:
            scalars["unit"]["levels"]["bandgap"] = "eV"
        if "bandgap_type" in scalars["levels"]:
            scalars["unit"]["levels"]["bandgap_type"] = "unitless"
        # Store KS states
        assembly.attach("arbit","kohn_sham_eigenvalues",ks_states)
        assembly["kohn_sham_eigenvalues"].attrs["order"] = ["state","occupation","eigenvalue"]
        assembly["kohn_sham_eigenvalues"].attrs["unit"] =  ["unitless","electrons","eV"]
        # Store final forces
        if total_forces:
            assembly.attach("n_vector","forces",total_forces, unit="eV/angstroem")
            assembly.attach("n_vector","forces~hellmann_feynman",hellmann_feynman_forces, unit="eV/angstroem")
            assembly.attach("n_vector","forces~ionic",ionic_forces, unit="eV/angstroem")
            assembly.attach("n_vector","forces~multipole",multipole_forces, unit="eV/angstroem")
            assembly.attach("n_vector","forces~pulay_gga",pulay_gga_forces, unit="eV/angstroem")
            assembly.attach("n_vector","forces~vdw",forces_vdw, unit="eV/angstroem")
        if residual_forces:
            assembly.attach("arbit","evolution~residual_forces",residual_forces, dtype="float", unit="eV/angstroem")
        if len(positions) > 1: # i.e. geometry relaxation took place
            assembly.attach("arbit","positions~history",positions, dtype="float", unit="angstroem")
            assembly["positions~history"].attrs["coord_system"] = "cartesian"
        assembly["positions"] = positions[-1]
        was_single_point = True if len(scf_result["evolution~iteration_time"]) == 1 else False
        # convert un-even arrays into uniformly shaped ones
        if not scf_result["energy~vdw_correction"][-1]:
            del scf_result["energy~vdw_correction"]
        for k in scf_result.keys():
            if k.startswith("evolution"):
                scf_result[k] = lol_2_np(scf_result[k])
        if was_single_point:
            for k in scf_result.keys():
                scf_result[k] = scf_result[k][-1]
                if k.startswith("evolution"):
                    if k == "evolution~delta_forces" and np.isnan(scf_result[k]).all():
                        continue
                    assembly.attach("arbit", k, scf_result[k], unit=scf_result_unit[k])
                elif k.startswith("energy~"):
                    scalars["energies"][k[7:]] = scf_result[k]
                    scalars["unit"]["energies"][k[7:]] = scf_result_unit[k]
        else:
            for k in scf_result.keys():
                assembly.attach("arbit", k, scf_result[k], unit=scf_result_unit[k])
                if k.startswith("energy~"):
                    scalars["energies"][k[7:]] = scf_result[k][-1]
                    scalars["unit"]["energies"][k[7:]] = scf_result_unit[k]
        assembly["positions"].attrs["unit"] = "angstroem"
        assembly["positions"].attrs["coord_system"] = "cartesian"
        # Post processing
        if "non_metal_total" in scalars["energies"] and "metal_total" in scalars["energies"]:
            scalars["energy"] = scalars["energies"]["non_metal_total"]
            if assembly.dimensions is not None:
                not_metals = (1,2,5,6,7,8,9,10,14,15,16,17,18,32,33,34,35,36,51,52,53,54,85,86)
                count_metal = 0
                for z in assembly.atoms["Zs"]:
                    if z not in not_metals:
                        count_metal += 1
                if count_metal > 5:
                    scalars["energy"] = scalars["energies"]["metal_total"]
                    print("assembly has periodic metal. Using metal energy.")
                elif count_metal:
                    scalars["energy"] = None
                    print("assembly is periodic and has metal. But can't determine which energy to use.")
                else:
                    print("assembly has no metal. Using non-metal energy.")
        
        if "hirshfeld" in scalars["analysis"]["kinds"] and not "MBD-rsscs" in scalars["analysis"]["kinds"] and scalars["energies"]["vdw_correction"]:
            scalars["analysis"]["kinds"].add("most-likely TS-vdW")
        if "num_cubes" in scalars["stats"] and scalars["stats"]["num_cubes"]:
            scalars["analysis"]["kinds"].add("cubes")
        scalars["analysis"]["kinds"] = list(scalars["analysis"]["kinds"])
        scalars["information"]["folderpath"] = str(filepath.parent.resolve())
        assembly.data.attrs.update(scalars)
        if not scalars["information"]["success"]:
            print("!!! FHI-aims calculation is not having a nice day !!!")
    return assembly
